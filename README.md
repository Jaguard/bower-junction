# bower-junction
Alternative [Bower](http://bower.io) [link](http://bower.io/docs/api/#link)/[unlink](http://bower.io/docs/api/#link) command line interface (CLI) scripts for Bower on unsupported Windows (XP,2000,2003) using [SysInternals Junction](http://technet.microsoft.com/en-us/sysinternals/bb896768.aspx).

## Getting started
Install **`bower-junction`** globally and you'll have access to the **`blink`** & **`bunlink`** commands anywhere on your Windows system. 
As a side effect of the global installation, the [npm-junction](https://npmjs.org/package/npm-junction) module will also be installed, similarly injecting NPM **`nlink`** & **`nunlink`** global commands.

```shell
npm install -g bower-junction
```
## **`blink`** command/script 
Creates a junction/symlink for the current directory or package (if a **`bower.json`** exists) into the Bower global links [1] or a reverse link for the specified package [2]

*Usage*:
--------
- **[1]** blink - create a symlink for the **`[current directory]`** or package (if a **`bower.json`** exists) under Bower global links
- **[2]** blink package - create a symlink into **`[current directory]/bower_components/[package]`** from the [package] link in Bower global links

## **`bunlink`** command/script
unlink the junction/symlink for the current directory or package (if a **`bower.json`** exists) from the Bower global links [1] or a remove a local link for the specified package [2]

*Usage*:
--------
- **[1]** bunlink - delete the package symlink for the **`[current directory]`** or package (if a **`bower.json`** exists) from the Bower global links
- **[2]** bunlink package - delete the symlink from **`[current directory]/bower_components/[package]`**

## **`blink`** & **`bunlink`** commands/scripts usage in packages **`package.json`** script/hooks
```javascript
    // ..
	"scripts": {
		"postinstall": "blink", // link current package into Bower global links
    "preinstall": "bunlink package", // unlink the specified global-linked package from the current package (to avoid npm install issues)
		"postinstall": "blink package" // link the specified global-linked package from the current package
	},
    // ..
```

## bower-junction changelog

- v1.0.0 (2013.10.13)
+ Add current package name extraction from `bower.json`
* Update documentation with `package.json` script/hooks linking/unlinking

- v0.0.1 (2013.05.03)
 + Initial commit