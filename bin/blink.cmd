:: -------------------------------------------------------------------------------------------------------------------------------
:: Create a junction/symlink for the current directory/package into the Bower global links [1] or a reverse link for the specified package [2]
:: Usage:
:: [1] blink        - create a symlink for the current directory under Bower global links
:: [2] blink package - create a symlink into current dir/bower_components/<package> from the <package> link in Bower global links
:: -------------------------------------------------------------------------------------------------------------------------------
@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET BOWER_LINKS=%APPDATA%\bower\data\links
SET JUNCTION=%~dp0..\..\npm-junction\bin\junction.exe
SET CWD=%CD%
:: create the Bower packages if not found
IF NOT EXIST "%JUNCTION%" (
	ECHO junction.exe not found in: %JUNCTION%
	SET ERRORLEVEL=-1
	GOTO :EOF
)
:: check if a package is defined as first param
IF NOT "%1"=="" GOTO :REVERSE
:: -----------------------------
:: [1] create a symlink for the current directory/package under Bower global links
:: -----------------------------
IF NOT EXIST "bower.json" (
	:: get package name=parent directory
	FOR /D %%I IN ("%CWD%") DO SET PACKAGE=%%~nxI
) ELSE (
	:: get package from bower.json
	FOR /F %%I IN ('node -p "require('./bower.json').name"') DO SET PACKAGE=%%~nxI
)
SET GLOBAL_LINK=%BOWER_LINKS%\%PACKAGE%
:: remove the previous link if exists
IF EXIST "%GLOBAL_LINK%" (
	"%JUNCTION%" -d "%GLOBAL_LINK%" > nul
	IF EXIST "%GLOBAL_LINK%" (
		rmdir /q /s "%GLOBAL_LINK%"
	)
)
:: create the global Bower packages if not create yet
IF NOT EXIST "%BOWER_LINKS%" (
	mkdir "%BOWER_LINKS%"
)
:: create the direct link
"%JUNCTION%" "%GLOBAL_LINK%" "%CWD%" > nul
ECHO Link [%PACKAGE%@%CWD%] =^> %GLOBAL_LINK% 
GOTO :EOF

:: -----------------------------
:: [2] create a symlink for the current directory under Bower global links
:: -----------------------------
:REVERSE
:: get the package name from param
SET PACKAGE=%1
SET GLOBAL_LINK=%BOWER_LINKS%\%PACKAGE%
:: create the Bower packages if not found
IF NOT EXIST "%GLOBAL_LINK%" (
	ECHO [%PACKAGE%] is not globally registered; %GLOBAL_LINK% not found!
	SET ERRORLEVEL=-2
	GOTO :EOF
)
SET LOCAL_PACKAGES=%CWD%\bower_components
:: create the global Bower packages if not create yet
IF NOT EXIST "%LOCAL_PACKAGES%" (
	mkdir "%LOCAL_PACKAGES%"
)
SET LOCAL_LINK=%LOCAL_PACKAGES%\%PACKAGE%
:: delete local link if exists already
IF EXIST "%LOCAL_LINK%" (
	"%JUNCTION%" -d "%LOCAL_LINK%" > nul
	IF EXIST "%LOCAL_LINK%" (
		rmdir /q /s "%LOCAL_LINK%"
	)
	ECHO Ublink existing [%PACKAGE%] ^<= %LOCAL_LINK%
)
:: create the reverse symlink
"%JUNCTION%" "%LOCAL_LINK%" "%GLOBAL_LINK%" > nul
ECHO Re-link [%PACKAGE%@%LOCAL_LINK%] =^> %GLOBAL_LINK%
GOTO :EOF