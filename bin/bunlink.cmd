:: -------------------------------------------------------------------------------------------------------------------------------
:: Unlink the junction/symlink for the current directory from the Bower global links [1] or a remove a local link for the specified package [2]
:: Usage:
:: [1] bunlink - delete the package symlink for the <current directory> from the Bower global links
:: [2] bunlink package - delete the symlink from <current directory>/bower_components/<package>
:: -------------------------------------------------------------------------------------------------------------------------------
@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SET BOWER_LINKS=%APPDATA%\bower\data\links
SET JUNCTION=%~dp0..\..\npm-junction\bin\junction.exe
SET CWD=%CD%
:: create the Bower packages if not found
IF NOT EXIST "%JUNCTION%" (
	ECHO junction.exe not found in: %JUNCTION%
	SET ERRORLEVEL=-1
	GOTO :EOF
)
:: check if a package is defined as first param
IF NOT "%1"=="" GOTO :LOCAL
:: -----------------------------
:: [1] delete the package symlink for the current directory from the Bower global links
:: -----------------------------
IF NOT EXIST "bower.json" (
	:: get package name=parent directory
	FOR /D %%I IN ("%CWD%") DO SET PACKAGE=%%~nxI
) ELSE (
	:: get package from bower.json
	FOR /F %%I IN ('node -p "require('./bower.json').name"') DO SET PACKAGE=%%~nxI
)
SET GLOBAL_LINK=%BOWER_LINKS%\%PACKAGE%
:: remove the previous link if exists
IF EXIST "%GLOBAL_LINK%" (
	:: try to remove the link with junction, otherwise remove as dir
	"%JUNCTION%" -d "%GLOBAL_LINK%" > nul
	IF EXIST "%GLOBAL_LINK%" (
		ECHO %GLOBAL_LINK% is not a symlink/junction!
  ) ELSE (
		ECHO Unlink [%PACKAGE%@%CWD%] ^<= %GLOBAL_LINK%
  ) 
) ELSE (
	ECHO [%PACKAGE%] is not a global package =^> %GLOBAL_LINK% ^(?^)
)
GOTO :EOF

:: -----------------------------
:: [2] delete the symlink from <current directory>/bower_components/<package>
:: -----------------------------
:LOCAL
:: get the package name from param
SET PACKAGE=%1
SET LOCAL_LINK=%CWD%\bower_components\%PACKAGE%
:: remove the local link if exists
IF EXIST "%LOCAL_LINK%" (
	:: try to remove the link with junction, otherwise remove as dir
	"%JUNCTION%" -d "%LOCAL_LINK%" > nul
	IF EXIST "%LOCAL_LINK%" (
		ECHO %LOCAL_LINK% is not a symlink/junction, remove as dir!
		rmdir /q /s "%LOCAL_LINK%"
	)
	ECHO Unlink [%PACKAGE%] ^<= %LOCAL_LINK%
) ELSE (
	ECHO [%PACKAGE%] is not a local package =^> %LOCAL_LINK% ^(?^)
)
GOTO :EOF