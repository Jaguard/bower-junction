bower-junction changelog
======================

v1.0.0 (2013.12.14)
-------------------
+ Add current package name extraction from `bower.json`
* Update documentation with `package.json` script/hooks linking/unlinking

v0.0.1 (2013.11.12)
-------------------
+ Initial release